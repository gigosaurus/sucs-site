<h2>Using VLC</h2>
<p>We recommend that you use the latest stable version of VLC, which is available from <a href="http://www.videolan.org/vlc">http://www.videolan.org/vlc</a></p>
<p>Once VLC is installed:</p>
<ul>
<li>Select "Open Network Stream..." from the Media menu</li>
<li>Enter one of the following options in the URL box:
<ul>
<li>
<tt>http://stream.sucs.org/high.flv</tt>
for high quality (576x432, 1088 kbit/s)
</li>
<li>
<tt>http://stream.sucs.org/low.flv</tt>
for low quality (384x288, 576 kbit/s)</li>
</ul>
If the stream appears to lock up, stop the stream and then restart</li>
</ul>
<p>Other media players that support FLV should be able to play the stream using the above address. </p>
<p>If you have another player that works, feel free to email instructions to <a href="mailto:admin@sucs.org">admin@ </a>and we'll add them to the list</p>