<p>We have put together a set of tools to make it easier to access SUCS services from your own computer. Note that you don't have to use the SUCS Tools to use our services - they're just a time-saving way to do so. The tools consist of the following:</p>

<ul>
<li><a href="/Tools/Desktop%20on%20Demand">Desktop on Demand (VNC)</a></li>
<li><a href="/Community/Milliways">Milliways chat rooms</a></li>
<li><a href="/Knowledge/Help/SUCS%20Services/Logging%20in%20remotely">SUCS remote shell (SSH)</a></li>
<li><a href="/">SUCS Website</a></li>
<li><a href="/Knowledge/Help/SUCS%20Services/Accessing%20your%20email">Your Email</a></li>
<li><a href="/Knowledge/Help/SUCS%20Services/Using%20WebDAV">Your Files (WebDAV)</a></li>
</ul>

<p>For more information about the services, click on the links.</p>

<h2>Downloading and installing the tools</h2>
<p>There are two versions of the tools. Which one you need depends on which operating system you use - <a href="#windows">Windows</a> or <a href="#macosx">Mac OS X</a>. If you use Linux, you probably already have an SSH and a VNC client installed and so don't need the SUCS Tools.</p>

<a name="windows"></a>
<h3>Windows</h3>
<ol>
<li><a href="/files/sucstools-latest.exe">Download SUCS Tools for Windows</a></li>
<li>Once the file has downloaded, double-click on it to start installing the SUCS Tools. A dialog box might appear telling you that "the publisher could not be verified." This is because we haven't paid for a digital certificate to sign the file with. Click "Run" to start the installation.</li>
<li>Follow the instructions provided by the installer.</li>
</ol>
<p>When the installation has completed, you can access the SUCS Tools via Start > All Programs > SUCS Tools.</p>
<a name="macosx"></a>
<h3>Mac OS X</h3>
<ol>
<li><a href="/files/SUCS-latest.dmg">Download SUCS Tools for Mac OS X</a></li>
<li>Once the file has downloaded, it might automatically open (it depends which browser you use). If it doesn't, double click on the file to mount the disk image.</li>
<li>Drag the SUCS folder to the Applications shortcut to install the tools.</li>
</ol>
<p>When the files have finished copying, you will find the SUCS Tools in your Applications folder.</p>