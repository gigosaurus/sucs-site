<p>If you wish to use our games server, you must configure your web browser to connect directly using no proxy server for SSL connections.</p><p>See <a href="https://sucs.org/Knowledge/Help/SUCS%20Services/Configuring%20Proxy%20Settings#firefox" title="Firefox instructions">instructions for Firefox</a> or instructions for <a href="https://sucs.org/Knowledge/Help/SUCS%20Services/Configuring%20Proxy%20Settings#ie" title="Internet Explorer instructions">Internet Explorer</a> </p>

<h2><a name="firefox" title="firefox"></a>Firefox</h2><ol><li>Open the Firefox Options window by selecting &quot;<strong>Tools</strong>, <strong>Options...</strong>&quot; from the menu bar<br />
<img alt="Tools-&gt;Options menu bar entry" src="/pictures/screenshots/proxy/firefoxmenubar.png" /><br /><br />
</li>
<li>Click the &quot;<strong>Advanced</strong>&quot; button and select the &quot;<strong>Network</strong>&quot; tab, then click the &quot;<strong>Settings...</strong>&quot; button<br />
<img alt="Advanced-&gt;Network tab of Firefox options dialog" src="/pictures/screenshots/proxy/firefoxoptions.png" />
</li>
<li><strong>Uncheck</strong> the &quot;<em>use this proxy server for all protocols</em>&quot; box, and <strong>delete</strong> the contents of the &quot;<em>SSL Proxy</em>&quot; field, so that it&#39;s blank<br />
<img alt="Firefox connection settings" src="/pictures/screenshots/proxy/firefoxconnectionsettings.png" />
</li>
<li>That&#39;s it! Click OK, and you should be able to access the SUCS Games Server page, in addition to normal web pages</li>
</ol>

<h2><a name="ie" title="ie"></a>Internet Explorer</h2>
<ol>
<li>Open Windows Control Panel (Start-&gt;Control Panel)</li>
<li>Select the <strong>Network and Internet Connections</strong> category<br />
<img src="/pictures/screenshots/proxy/iecontrolpanel.png" />
</li>
<li>Click <strong>Internet Options</strong><br />
<img src="/pictures/screenshots/proxy/ienetworkinetsettings.png" />
</li>
<li>On the <strong>Connections</strong> tab, select <strong>LAN Settings...</strong><br />
<img src="/pictures/screenshots/proxy/ieinternetproperties.png" />
</li>
<li>Click the <strong>Advanced...</strong> button <br />
<img src="/pictures/screenshots/proxy/ielansettings.png" />
</li>
<li><strong>Untick</strong> the <em>Use the same proxy server for all protocols</em> checkbox, and <strong>delete</strong> the text from the <em>Secure</em> field<br />
<img src="/pictures/screenshots/proxy/ieproxysettings.png" />
</li>
<li>That&#39;s it! Click OK, and you should be able to access the SUCS Games Server page, in addition to normal web pages</li>
</ol>

<p>Any further problems, please contact us at <strong>games@sucs.org</strong> !</p>