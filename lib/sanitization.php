<?
function sanitizePhone($phone)
{
    return preg_replace("/[ ()]/", "", $phone);
}

function sanitizeAddress($address)
{
    return str_replace(array("\r\n", "\r"), array("\n", "\n"), $address);
}

function sanitizePostcode($postcode)
{
    //force uppercase, remove any possible stupid spaces and add the single space in the correct place
    $postcode = strtoupper($postcode);
    $postcode = str_replace(" ", "", $postcode);
    return substr_replace($postcode, " ", -3, 0);
}

?>
