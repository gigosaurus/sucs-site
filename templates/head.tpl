<!doctype html>
<html lang="{$language.code}">

<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="theme-color" content="#e65c00">
    <title>{$title} - SUCS</title>
    <link rel="SHORTCUT ICON" href="{$baseurl}/favicon.ico"/>
    <link rel="apple-touch-icon" href="{$baseurl}/images/apple-touch-icon.png"/>

    <script type="text/javascript" src="{$baseurl}/videos/talks/ufo.js"></script>

    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/common.css" media="screen,print"/>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/sucs.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/box.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/downloads.css" media="screen"/>
    {if isset($extra_styles)}
        {foreach from=$extra_styles item=style}
            <link rel="stylesheet" type="text/css" href="{$style}" media="screen"/>
        {/foreach}
    {/if}
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/print.css" media="print"/>

    {if isset($rss_url)}
        <link rel="alternate" type="application/rss+xml" title="{$title}" href="{$rss_url}"/>
    {/if}
    {if isset($atom_url)}
        <link rel="alternate" type="application/atom+xml" title="{$title}" href="{$atom_url}"/>
    {/if}
    <meta name="description"
          content="Swansea University Computer Society - providing student computing facilities and personal web pages."/>
    {if $refresh}
        <meta http-equiv="REFRESH" content="{$refresh}" />{/if}
    
    <meta name="viewport" content="width=device-width">

    {if isset($extra_scripts)}
        {foreach from=$extra_scripts item=script}
            {$script}
        {/foreach}
    {/if}

    <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/ie.css"/>
    <script type="text/javascript" src="{$baseurl}/js/cb.js"></script>
    <![endif]-->
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/ie8.css"/>
    <![endif]-->

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="{$baseurl}/css/ielt8.css" media="screen"/>
    <![endif]-->

</head>
<body>

  <div id="footer-fix">
    {include file="branding.tpl"}

    <div id="wrapper">
	<div id="content">
	  {include file="usermessages.tpl"}
